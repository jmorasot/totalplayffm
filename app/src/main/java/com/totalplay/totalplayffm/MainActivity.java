package com.totalplay.totalplayffm;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Pair;
import android.widget.Button;
import android.widget.Toast;

import com.identy.IdentySdk;
import com.identy.WSQCompression;
import com.identy.encryption.FileCodecBase64;
import com.identy.enums.Finger;
import com.identy.enums.Hand;
import com.identy.enums.Template;
import com.neemtecsolutions.fingerreader.AppUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private String mLicenseId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLicenseId = "totalplayffmv2@totalplay.com.lic";
        Button buttonVerify = findViewById(R.id.act_main_verify);
        Button buttonEnroll = findViewById(R.id.act_main_enroll);
        buttonVerify.setOnClickListener(view -> initLogin(true));
        buttonEnroll.setOnClickListener(view -> initLogin(false));
    }

    public void initLogin(boolean isVerify) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED  ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED  ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED  ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_BOOT_COMPLETED) != PackageManager.PERMISSION_GRANTED ) {

            String [] permissions = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECEIVE_BOOT_COMPLETED
            };
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
        } else {
            String user = "FFMUSER1";
            if (isVerify)
                verifyUser(user);
            else
                enrollUser(user);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (!(grantResults.length > 0)) {
                Toast.makeText(getApplicationContext(), "Acepta permiso", Toast.LENGTH_LONG).show();
            } else {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getApplicationContext(), "Acepta permiso", Toast.LENGTH_LONG).show();
                        break;
                    }
                }
            }
        }
    }

    public void verifyUser(String user) {
        IdentySdk.IdentyResponseListener responseListener = initListener("/verify_output_" + user);
        try {
            IdentySdk.InitializationListener initializationListener = sdk -> {
                try {
                    ArrayList<Template> requiredTemplates = new ArrayList<>();
                    requiredTemplates.add(Template.WSQ);
                    requiredTemplates.add(Template.RAW);
                    ((IdentySdk) sdk).setMode("commercial")
                            .setAS(true)
                            .setRequiredTemplates(requiredTemplates)
                            .displayImages(true)
                            .setWSQCompression(WSQCompression.WSQ_10_1)
                            .verify();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };

            boolean response = IdentySdk.newInstance(this, mLicenseId, initializationListener, responseListener);
            if (!response) {
                Toast.makeText(this, "Error, usuario no encontrado", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enrollUser(String user){
        IdentySdk.IdentyResponseListener responseListener = initListener("/enroll_output_" + user);
        try {
            IdentySdk.InitializationListener initializationListener = sdk -> {
                try {
                    ArrayList<Template> requiredTemplates = new ArrayList<>();
                    requiredTemplates.add(Template.WSQ);
                    requiredTemplates.add(Template.RAW);
                    ((IdentySdk) sdk).setMode("commercial")
                            .setAS(true)
                            .setRequiredTemplates(requiredTemplates)
                            .displayImages(true)
                            .setWSQCompression(WSQCompression.WSQ_10_1)
                            .enroll();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };

            IdentySdk.newInstance(this, mLicenseId, initializationListener, responseListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private IdentySdk.IdentyResponseListener initListener(String path) {
        return new IdentySdk.IdentyResponseListener() {
            @Override
            public void onResponse(IdentySdk.IdentyResponse identyResponse) {
                JSONObject jsonObject = identyResponse.toJson();

                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(AppUtils.cleanExternalDirectory("json_output").toString() + path);
                    fileOutputStream.write(jsonObject.toString().getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                for (Map.Entry<Pair<Hand, Finger>, IdentySdk.FingerOutput> entry : identyResponse.getPrints().entrySet()) {
                    Pair<Hand, Finger> handFingerPair = entry.getKey();
                    IdentySdk.FingerOutput fingerOutput = entry.getValue();

                    String directory = AppUtils.createExternalDirectory("WSQ_OUTPUT ") + "/";
                    File file = new File(directory, handFingerPair.first.toString() + handFingerPair.second.toString() + ".wsq");
                    if (file.exists()) {
                        file.delete();
                    }
                    try {
                        FileOutputStream outputStream = new FileOutputStream(file);
                        String decodedString = fingerOutput.getTemplates().get(Template.WSQ);
                        outputStream.write(FileCodecBase64.decode(decodedString));
                        outputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onErrorResponse(IdentySdk.IdentyError identyError) {
                Toast.makeText(getApplicationContext(), identyError.toString(), Toast.LENGTH_LONG).show();
            }
        };
    }
}